const Restaurants=require('../models/restaurant')

exports.getAllRestaurants=(req,res)=>{
    Restaurants.find().then(
     result=>{
        res.status(200).json({ 
            message:"restaurants fetched successfully" , 
            restaurantList:result
          })
     }
    ).catch(
      error=>{
        res.status(500).json({ 
            message:"Error in database" , 
            error:error
          })
      }

    )
}


exports.getRestaurantByName=(req,res)=>{
    const filter={name:req.params.name}

    Restaurants.find(filter).then(
        result=>{
           res.status(200).json({ 
               message:"restaurants fetched successfully" , 
               restaurantList:result
             })
        }
       ).catch(
         error=>{
           res.status(500).json({ 
               message:"Error in database" , 
               error:error
             })
         }
   
       )

}

exports.getRestaurantsByFilter=(req,res)=>{
    
}