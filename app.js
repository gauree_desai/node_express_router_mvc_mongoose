//import
const express=require('express')
const bodyParser=require('body-parser')
var cors = require('cors')
const restaurantRoutes=require('./routes/restaurants')
const locationRoutes=require('./routes/locations')
const mongoose=require('mongoose')

//create server
var app=express()

//constants
const PORT= 6767
const log=console.log

//connect to mongoDB 
const MONGO_URI="mongodb://localhost/zomato_30";
mongoose.connect(MONGO_URI,
  ()=>{
  console.log("MongoDB connected....")},
  e=>console.log("error occured",e))




//middleware
app.use(cors())
app.use(bodyParser.json())
app.use('/restaurant',restaurantRoutes)
app.use('/location',locationRoutes)







//last stmt listen
app.listen(PORT,()=>{
  log(`app is running on ${PORT}`)
})